import { Sequelize, Model, DataTypes } from 'sequelize';

// Initialize a new Sequelize instance
const sequelize = new Sequelize(process.env.DB_NAME!, process.env.DB_USERNAME!, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: 'postgres',
    logging: process.env.ENVIRONMENT === 'development' ? console.log : false,
});

class User extends Model implements User {};
User.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: DataTypes.STRING,
    createdAt: DataTypes.DATE,
}, { sequelize, modelName: 'user' });

class UserTelegram extends Model {
    id!: number;
    telegramId!: string;
    userId!: number;
}
UserTelegram.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    telegramId: DataTypes.STRING,
    userId: DataTypes.INTEGER,
}, { sequelize, modelName: 'userTelegram' });

class UserEmail extends Model {
    id!: number;
    email!: string;
    userId!: number;
}
UserEmail.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    email: DataTypes.STRING,
    userId: DataTypes.INTEGER,
}, { sequelize, modelName: 'userEmail' });

class Queue extends Model {
    id!: number;
    name!: string;
    createdAt!: Date;
}
Queue.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: DataTypes.STRING,
    createdAt: DataTypes.DATE,
}, { sequelize, modelName: 'queue' });

class UserQueueAuthenticationStatus extends Model {
    id!: number;
    userId!: number;
    queueId!: number;
}
UserQueueAuthenticationStatus.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    userId: DataTypes.INTEGER,
    queueId: DataTypes.INTEGER,
}, { sequelize, modelName: 'userQueueAuthenticationStatus' });

class Ticket extends Model {
    id!: number;
    title!: string;
    topic!: string;
    createdAt!: Date;
    channelId!: string;
}
Ticket.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    title: DataTypes.STRING,
    initialTopic: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    channelId: DataTypes.STRING,
}, { sequelize, modelName: 'ticket' });

// Defining associations
UserQueueAuthenticationStatus.belongsTo(User, { foreignKey: 'userId' });
UserQueueAuthenticationStatus.belongsTo(Queue, { foreignKey: 'queueId' });
Queue.hasMany(UserQueueAuthenticationStatus, { foreignKey: 'queueId' });
User.hasMany(UserTelegram, { foreignKey: 'userId' });
UserTelegram.belongsTo(User, { foreignKey: 'userId' });
User.hasMany(UserEmail, { foreignKey: 'userId' });
UserEmail.belongsTo(User, { foreignKey: 'userId' });

// Export models and Sequelize connection
export { User, Queue, UserQueueAuthenticationStatus, Ticket, sequelize };

// Function to initialize database and sync models
export const initialize = async () => {
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
        await sequelize.sync({ force: false });
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
};
