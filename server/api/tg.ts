import TelegramBot from "node-telegram-bot-api";
import { Ticket, initialize } from "../database";
const token = process.env.TG_TOKEN!;

let initialized = false;

const bot = new TelegramBot(token);

enum PendingAction {
    OpenOrReplyTicket,
    SendTicketTopic,
    SendTicketId
}

const ticketTopics = ['Urgent', 'Logistique', 'Administratif', 'Facturation', 'Accréditation', 'I.T.', 'Odoo', 'Truffe', 'Autre'];
const userPendingAction = new Map<String, PendingAction>();
const pendingTicketContent = new Map<String, string>();
const pendingDeletionTicketIds = new Map<String, number[]>();

export default defineEventHandler(async (event) => {

    console.log('Received event', event);

    if (!initialized) {
        initialized = true;
        initialize();
    }

    const body = await readBody(event);
    console.log(body);

    /*await bot.sendMessage(1018128258, 'Bienvenue, Simon\\.\n\nEn cas de problème technique, demande quelconque liée à l\'AGEPoly, vous pouvez ouvrir une demande via ce bot Telegram\\.\n\nPour commencer, cliquez sur le bouton ci\\-dessous \\!\n\n_Swipe to answer a ticket_', {
        disable_notification: true,
        parse_mode: 'MarkdownV2',
        reply_markup: {
            remove_keyboard: true,
            inline_keyboard: [[
                {
                    text: 'Nouveau ticket',
                    web_app: {
                        url: process.env.HOST_URL
                    }
                },
                {
                    text: 'Espace routage',
                    web_app: {
                        url: process.env.HOST_URL + '/routing?jwt=1234&source=tg'
                    }
                }
            ]]
        }
    });*/

    const isPrivateChat = body.message.chat.type === 'private';

    if (isPrivateChat) {

        const text = body.message.text;
        const firstName = body.message.chat.first_name;

        if (text) {

            switch (userPendingAction.get(body.message.chat.id)) {

                case PendingAction.SendTicketTopic:
                    if (ticketTopics.includes(text)) {
                        const ticket = await Ticket.create({
                            title: pendingTicketContent.get(body.message.chat.id),
                            topic: text,
                            createdAt: new Date()
                        });
                        bot.sendMessage(body.message.chat.id, "Demande de booking cargo\n\nVous \\: Bonjour\\, je suis Zac et j\\'aimerais emprunter un cargo\\.", {
                            disable_notification: true,
                            parse_mode: 'MarkdownV2',
                            reply_markup: {
                                remove_keyboard: true,
                                inline_keyboard: [
                                    [{
                                        text: 'Statut : en attente 🟠',
                                        callback_data: 'accept'
                                    },{
                                        text: 'Marquer comme résolu ✅',
                                        callback_data: 'accept'
                                    }]
                                ]
                            }
                        });

                        //const topic = await bot.createForumTopic(process.env.TG_FACTURATION_CHANNEL_ID!, `#${ticket.dataValues.id}`);

                        bot.sendMessage(body.message.chat.id, 'Votre ticket a été ouvert avec succès.')
                        .then((msg) => {
                            setTimeout(() => {
                                bot.deleteMessage(body.message.chat.id, msg.message_id);
                            }, 20_000);
                        })
                        pendingDeletionTicketIds.get(body.message.chat.id)!.push(body.message.message_id);
                        pendingDeletionTicketIds.get(body.message.chat.id)!.forEach((id) => {
                            bot.deleteMessage(body.message.chat.id, id);
                        });

                    }
                    break;

                case PendingAction.OpenOrReplyTicket:
                    pendingDeletionTicketIds.get(body.message.chat.id)!.push(body.message.message_id);
                    if (text === 'Ouvrir un ticket') {
                        bot.sendMessage(body.message.chat.id, 'Très bien, sélectionnez la thématique de votre demande.', {
                            reply_markup: {
                                // subset of 3
                                keyboard: ticketTopics.map((topic) => [{ text: topic }]),
                                resize_keyboard: true
                            }
                        }).then((msg) => {
                            pendingDeletionTicketIds.get(body.message.chat.id)!.push(msg.message_id);
                        });
                        userPendingAction.set(body.message.chat.id, PendingAction.SendTicketTopic);
                    }
                    else if (text === 'Répondre à un ticket') {
                        bot.sendMessage(body.message.chat.id, 'Veuillez saisir l\'ID du ticket auquel vous souhaitez répondre')
                        .then((msg) => {
                            pendingDeletionTicketIds.get(body.message.chat.id)!.push(msg.message_id);
                        });
                        userPendingAction.set(body.message.chat.id, PendingAction.SendTicketId);
                    }
                    break;

                default:

                    pendingTicketContent.set(body.message.chat.id, text);
                    userPendingAction.set(body.message.chat.id, PendingAction.OpenOrReplyTicket);
                    pendingDeletionTicketIds.set(body.message.chat.id, [body.message.message_id]);

                    bot.sendMessage(body.message.chat.id, `Bonjour ${firstName}, souhaitez-vous ouvrir un ticket ou répondre à un ticket existant ?`, {
                        reply_markup: {
                            keyboard: [
                                [{ text: 'Ouvrir un ticket' }],
                                [{ text: 'Répondre à un ticket' }]
                            ],
                            resize_keyboard: true
                        }
                    }).then((msg) => {
                        pendingDeletionTicketIds.get(body.message.chat.id)!.push(msg.message_id);
                    });

                    break;
            }
        }

    }

/*
    try {

        const body = await readBody(event);
        console.log(body);
        console.log(body.message);

        if (!body.message) {
            return {
                error: 'No message found'
            }
        }

        if (body.message.chat?.type === 'supergroup') {

            bot.sendMessage(body.message.chat.id, 'Subject\\: *needs triage*\n\nBonjour\\, je m\'appelle Zac et j\'aurais besoin de 200€ pour un event\\.', {
                message_thread_id: body.message.message_thread_id,
                parse_mode: 'MarkdownV2',
                reply_markup: {
                    inline_keyboard: [[
                        {
                            text: 'Update subject',
                            url: 'https://t.me/agepsupport_31janv_bot/supporttestopen?startapp=command'
                        },
                        {
                            text: 'Move to another queue',
                            callback_data: 'update_message'
                        }
                    ]]
                }
            });
            return;
        }

        bot.deleteMessage(body.message.chat.id, body.message.message_id);

        // parse json

        // send an inline mini app 
        // const chatId = msg.chat.id;
        // const url = `https://example.com`;
        // bot.sendGame(chatId, url);

        const timeout = body.message.text === 'a';

        setTimeout(async () => {
            await bot.sendMessage(body.message.chat.id, 'Bienvenue, Simon\\.\n\nEn cas de problème technique, demande quelconque liée à l\'AGEPoly, vous pouvez ouvrir une demande via ce bot Telegram\\.\n\nPour commencer, cliquez sur le bouton ci\\-dessous \\!\n\n_Swipe to answer a ticket_', {
                disable_notification: true,
                parse_mode: 'MarkdownV2',
                reply_markup: {
                    remove_keyboard: true,
                    inline_keyboard: [[
                        {
                            text: 'Nouveau ticket',
                            web_app: {
                                url: 'https://420b-2001-620-618-588-2-80b3-0-3f4.ngrok-free.app/'
                            }
                        },
                        {
                            text: 'Espace routage',
                            web_app: {
                                url: 'https://420b-2001-620-618-588-2-80b3-0-3f4.ngrok-free.app/'
                            }
                        }
                    ]]
                }
            });

            const tickets = [
                {
                    content: "Demande de booking cargo\n\nVous \\: Bonjour\\, je suis Zac et j\\'aimerais emprunter un cargo\\.",
                    btns: [
                        [{
                            text: "We\'re replying 🛟",
                            callback_data: 'accept'
                        }],
                        [{
                            text: 'Mark as solved ✅',
                            callback_data: 'reject'
                        },
                        {
                            text: 'Open web app 🌐',
                            callback_data: 'more'
                        }]
                    ]
                },
                {
                    content: "Demande de booking cargo\n\nVous \\: Bonjour\\, je suis Zac et j\\'aimerais emprunter un cargo\\.",
                    btns: [
                        [{
                            text: 'Please reply 🟠',
                            callback_data: 'accept'
                        }],
                        [{
                            text: 'Mark as solved ✅',
                            callback_data: 'reject'
                        },
                        {
                            text: 'Open web app 🌐',
                            callback_data: 'more'
                        }]
                    ]
                }
            ]

            for (const ticket of tickets) {
                await bot.sendMessage(body.message.chat.id, ticket.content, {
                    disable_notification: true,
                    parse_mode: 'MarkdownV2',
                    reply_markup: {
                        remove_keyboard: true,
                        inline_keyboard: ticket.btns
                    }
                });
            }

        }, timeout ? 2000 : 0);
        return {
            hello: 'world'
        }
    }
    catch (e) {
        console.log(e);
        return {
            error: e instanceof Error ? e.message : e
        }
    }
    */
})