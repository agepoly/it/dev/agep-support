export default defineNuxtRouteMiddleware(async (to, from) => {
    const store = useAppStore();
    if (!store.userData) await store.fetchUserData();
    if (!store.userData && to.path !== '/login') {
        return navigateTo('/login');
    }
    return;
});
