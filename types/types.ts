// this represents a ticket as stored in the database
export interface Ticket {
    id: number;
    title: string;
    userSelectedTopic: string;
    openedAt: Date;
}

export interface APIUnit {
    id: number;
    name: string;
    createdAt: Date;
}

export interface User {
    id: number;
    firstname: string;
    lastname: string;
    canRoute: boolean;
    whiskeyId: string;
}

export interface APIUser extends User {
    displayName: string;
};

export interface APITicket extends Ticket {
    initialDescription: string;
    units: APIUnit[];
    users: APIUser[];
}

// this represents a routing ticket sent to the client
export interface APIRoutingTicket extends APITicket {}

export interface APIUserTicket extends APITicket {}
