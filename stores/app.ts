import { defineStore } from 'pinia';
import type { APIRoutingTicket, APIUser, APIUserTicket, Ticket } from '~/types/types';

interface State {
    jwt: string;
    userData: APIUser;
    userTickets: APIUserTicket[];
    routingTickets: APIRoutingTicket[];
}

export const useAppStore = defineStore('appStore', {
    state: (): State => ({
        jwt: 'somejwt',
        userData: {
            id: 0,
            firstname: '',
            lastname: '',
            displayName: '',
            canRoute: false,
            whiskeyId: ''
        },
        userTickets: [],
        routingTickets: []
    }),
    getters: {
        getUserDisplayName(): String {
            return `${this.userData.firstname.charAt(0).toUpperCase()}. ${this.userData.lastname}`;
        }
    },
    actions: {
        setJwt(jwt: string) {
            this.jwt = jwt;
        },
        setUserData(userData: APIUser) {
            this.userData = userData;
        },
        setRoutingTickets(routingTickets: APIRoutingTicket[]) {
            this.routingTickets = routingTickets;
        },
        async fetchUserData() {
            const userData = {
                id: 10,
                firstname: 'Simon',
                lastname: 'Lefort',
                canRoute: true,
                whiskeyId: 'whiskey',
                displayName: 'Simon Lefort'
            }
            this.setUserData(userData);
        },
        async fetchTickets() {
            const date = new Date();
            const routingTickets: APIRoutingTicket[] = [
                {
                    id: 1,
                    title: '',
                    userSelectedTopic: 'URGENT',
                    initialDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    openedAt: new Date(date.setHours(date.getHours() - 1)),
                    users: [{ id: 1, firstname: 'Simon', lastname: 'Lefort', displayName: 'Simon Lefort', canRoute: true, whiskeyId: 'whiskey'}],
                    units: []
                },
                {
                    id: 2,
                    title: '',
                    userSelectedTopic: 'LOGIST.',
                    openedAt: new Date(date.setHours(date.getHours() - 2)),
                    initialDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    users: [],
                    units: []
                }
            ];
            this.setRoutingTickets(routingTickets);
        }
    }
});
