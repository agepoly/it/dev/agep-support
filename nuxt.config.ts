// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devServer: {
    host: '0',
    port: 3000
  },
  devtools: { enabled: true },

  nitro: {
    esbuild: {
      options: {
        tsconfigRaw: {
          compilerOptions: {
            experimentalDecorators: true
          }
        }
      }
    }
  },

  modules: ["@pinia/nuxt", "@nuxtjs/tailwindcss"]
})